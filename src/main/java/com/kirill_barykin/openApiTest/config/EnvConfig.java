package com.kirill_barykin.openApiTest.config;

public class EnvConfig {
    /**
     * Адреса обращения
     */
    private static final String BASE_URI = "https://reqres.in/";
    private static final String ENDPOINT_API = "api/";

    public static String URI_REQRES_API = BASE_URI + ENDPOINT_API;

    /**
     * Тестовые переменные
     */
    public static final String CREATE_USER_NAME = "Kir";
    public static final String CREATE_USER_JOB = "QA Automation Engineer";
}
