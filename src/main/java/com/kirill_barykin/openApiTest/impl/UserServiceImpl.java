package com.kirill_barykin.openApiTest.impl;

import com.kirill_barykin.openApiTest.models.User;
import com.kirill_barykin.openApiTest.models.userList.BaseUserList;
import com.kirill_barykin.openApiTest.services.UserService;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class UserServiceImpl implements UserService {
    private final String SERVICE = "users";

    /**
     * Выполнение запроса на получение списка пользователей
     *
     * @return - ответ в виде объекта BaseUserList (если статус ответа 200)
     */
    @Override
    public BaseUserList getUsersList() {
        final String PAGE_PARAM = "?page=";
        final int PAGE_NUMBER = 2;

        String url = URI + SERVICE + PAGE_PARAM + PAGE_NUMBER;

        Response response = given()
                .contentType(ContentType.JSON)
                .get(url);

        if (response.statusCode() == 200) {
            return response.as(BaseUserList.class);
        }
        return null;
    }

    /**
     * Выполнение запроса на создание пользователя
     *
     * @param user - тело запроса. Объект User
     * @return - ответ в виде объекта User (если статус ответа 201)
     */
    @Override
    public User createUser(User user) {
        String url = URI + SERVICE;

        Response response = given()
                .contentType(ContentType.JSON)
                .body(user)
                .post(url);

        if (response.statusCode() == 201) {
            return response.as(User.class);
        }
        return null;
    }
}
