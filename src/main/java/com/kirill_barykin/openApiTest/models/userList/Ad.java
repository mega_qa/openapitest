package com.kirill_barykin.openApiTest.models.userList;

import lombok.*;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Ad {
    private String company;
    private String url;
    private String text;
}
