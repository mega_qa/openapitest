
package com.kirill_barykin.openApiTest.models;

import lombok.*;

import java.util.Date;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {

    private String job;
    private String name;

    private String id;
    private Date createdAt;

    private String first_name;
    private String last_name;
    private String avatar;
    private String email;

}
