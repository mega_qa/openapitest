package com.kirill_barykin.openApiTest.services;

import com.kirill_barykin.openApiTest.models.User;
import com.kirill_barykin.openApiTest.models.userList.BaseUserList;

public interface UserService extends BasicService {

    BaseUserList getUsersList();

    User createUser(User user);

}
