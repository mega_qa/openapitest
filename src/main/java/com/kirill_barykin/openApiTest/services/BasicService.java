package com.kirill_barykin.openApiTest.services;

import com.kirill_barykin.openApiTest.config.EnvConfig;

public interface BasicService {
    String URI = EnvConfig.URI_REQRES_API;
}
