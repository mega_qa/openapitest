package com.kirill_barykin.openApiTest.tests;

import com.kirill_barykin.openApiTest.impl.UserServiceImpl;
import com.kirill_barykin.openApiTest.services.UserService;
import com.kirill_barykin.openApiTest.models.User;
import com.kirill_barykin.openApiTest.models.userList.BaseUserList;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Задача 1.1
 *
 * Получить список пользователей (GET  https://reqres.in/api/users?page=2),
 * замапить на объект и проверить, что все поля пришли (достаточно на notNull).
 */

public class GetUserListTest {
    UserService userService = new UserServiceImpl();

    @Test
    public void getUserListAndAssertNotNullEveryField() {

        BaseUserList createdUser = userService.getUsersList();

        Assert.assertNotNull(createdUser);
        Assert.assertNotNull(createdUser.getAd());
        Assert.assertNotNull(createdUser.getPage());
        Assert.assertNotNull(createdUser.getPer_page());
        Assert.assertNotNull(createdUser.getTotal());
        Assert.assertNotNull(createdUser.getTotal_pages());

        createdUser.getData().stream().map(User::getId).forEach(Assert::assertNotNull);
        createdUser.getData().stream().map(User::getEmail).forEach(Assert::assertNotNull);
        createdUser.getData().stream().map(User::getFirst_name).forEach(Assert::assertNotNull);
        createdUser.getData().stream().map(User::getLast_name).forEach(Assert::assertNotNull);
        createdUser.getData().stream().map(User::getAvatar).forEach(Assert::assertNotNull);

        Assert.assertNotNull(createdUser.getAd().getCompany());
        Assert.assertNotNull(createdUser.getAd().getUrl());
        Assert.assertNotNull(createdUser.getAd().getText());

    }
}
