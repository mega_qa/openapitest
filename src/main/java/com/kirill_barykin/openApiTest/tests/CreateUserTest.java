package com.kirill_barykin.openApiTest.tests;


import com.kirill_barykin.openApiTest.config.EnvConfig;
import com.kirill_barykin.openApiTest.impl.UserServiceImpl;
import com.kirill_barykin.openApiTest.models.User;
import com.kirill_barykin.openApiTest.services.UserService;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Задача 1.2
 *
 * Создать пользователя (POST https://reqres.in/api/users) с данными из примера на сайте:
 * подготовить тело запроса, отправить запрос с телом, замапить ответ на объект и проверить,
 * что в ответе те же самые значения из запроса.
 */

public class CreateUserTest {
    UserService userService = new UserServiceImpl();

    @Test
    public void createUserAndAssertFields() {

        User newUser = User.builder()
                .name(EnvConfig.CREATE_USER_NAME)
                .job(EnvConfig.CREATE_USER_JOB)
                .build();

        User createdUser = userService.createUser(newUser);

        Assert.assertNotNull(createdUser);
        Assert.assertNotNull(createdUser.getId());
        Assert.assertEquals(createdUser.getName(), EnvConfig.CREATE_USER_NAME);
        Assert.assertEquals(createdUser.getJob(), EnvConfig.CREATE_USER_JOB);

    }
}
